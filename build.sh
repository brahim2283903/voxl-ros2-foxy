#!/bin/bash
#
# builds everything without installing
#
# Modal AI Inc. 2023
# author: zachary.lowell@ascendengineer.com


set -e

AVAILABLE_PLATFORMS="qrb5165"

print_usage(){
    echo ""
    echo " Build the current project based on platform target."
    echo ""
    echo " Usage:"
    echo ""
    echo "  ./build.sh qrb5165"
    echo "        Build 64-bit binaries for qrb5165"
    echo ""
    echo ""
}



case "$1" in
    qrb5165)
        ROS_DIST="foxy"
        ;;

    *)
        print_usage
        exit 1
        ;;
esac

current_dir=$(pwd)

sudo apt purge --auto-remove -y cmake
sudo apt update
sudo apt install --fix-missing
sudo apt install -y python3-pip
sudo apt install -y software-properties-common
sudo apt install -y ca-certificates gpg wget
sudo apt-get update

# manually obtain copy of keyring
test -f /usr/share/doc/kitware-archive-keyring/copyright || wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | sudo tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null

echo "adding kitware repository to sources"
echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ bionic main' | sudo tee /etc/apt/sources.list.d/kitware.list >/dev/null
sudo apt-get update

#sudo apt-get install kitware-archive-keyring

sudo apt install -y cmake
sudo apt install -y qtcreator qtbase5-dev qt5-qmake
python3 -m pip install lark

ROS_PKG=ros_base
ROS_DISTRO=foxy
# Core ROS2 workspace - the "underlay"
ROS_BUILD_ROOT=/opt/ros/${ROS_DISTRO}-src
ROS_INSTALL_ROOT=/opt/ros/${ROS_DISTRO}

locale  # check for UTF-8

sudo apt update && sudo apt install locales
sudo locale-gen en_US en_US.UTF-8
sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
export LANG=en_US.UTF-8

# Add the ROS 2 apt repository
sudo apt-get update
sudo apt-get install -y --no-install-recommends \
                curl \
                wget \
                gnupg2 \
                lsb-release
sudo rm -rf /var/lib/apt/lists/*

wget --no-check-certificate https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc
sudo apt-key add ros.asc
sudo sh -c 'echo "deb [arch=$(dpkg --print-architecture)] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros2-latest.list'

# install development packages
sudo apt-get update
sudo apt-get install -y libeigen3-dev
sudo cp -r /usr/include/eigen3/Eigen /usr/include
sudo apt-get install -y --no-install-recommends \
                build-essential \
                git \
                libbullet-dev \
                libpython3-dev \
                python3-flake8 \
                python3-pip \
                python3-pytest-cov \
                python3-rosdep \
                python3-setuptools \
                python3-vcstool \
                python3-rosinstall-generator \
                python3-numpy \
		libasio-dev \
                libtinyxml2-dev \
                libcunit1-dev
sudo rm -rf /var/lib/apt/lists/*

# install some pip packages needed for testing
python3 -m pip install -U \
                colcon-common-extensions \
                argcomplete \
                flake8-blind-except \
                flake8-builtins \
                flake8-class-newline \
                flake8-comprehensions \
                flake8-deprecated \
                flake8-docstrings \
                flake8-import-order \
                flake8-quotes \
                pytest-repeat \
                pytest-rerunfailures \
                pytest

# compile yaml-cpp-0.6, which some ROS packages may use (but is not in the 18.04 apt repo)
git clone --branch yaml-cpp-0.6.0 https://github.com/jbeder/yaml-cpp yaml-cpp-0.6 && \
    cd yaml-cpp-0.6 && \
    mkdir build && \
    cd build && \
    cmake -DBUILD_SHARED_LIBS=ON .. && \
    make -j$(nproc) && \
    sudo cp libyaml-cpp.so.0.6.0 /usr/lib/aarch64-linux-gnu/ && \
    sudo ln -s /usr/lib/aarch64-linux-gnu/libyaml-cpp.so.0.6.0 /usr/lib/aarch64-linux-gnu/libyaml-cpp.so.0.6
    

# https://answers.ros.org/question/325245/minimal-ros2-installation/?answer=325249#post-id-325249
sudo mkdir -p ${ROS_BUILD_ROOT}/src && \
  cd ${ROS_BUILD_ROOT}
sudo sh -c "rosinstall_generator --deps --rosdistro ${ROS_DISTRO} ${ROS_PKG} launch_xml launch_yaml example_interfaces > ros2.${ROS_DISTRO}.${ROS_PKG}.rosinstall && \
cat ros2.${ROS_DISTRO}.${ROS_PKG}.rosinstall && \
    vcs import src < ros2.${ROS_DISTRO}.${ROS_PKG}.rosinstall"

# download unreleased packages
sudo sh -c "git clone --branch ros2 https://github.com/Kukanani/vision_msgs ${ROS_BUILD_ROOT}/src/vision_msgs && \
    git clone --branch ${ROS_DISTRO} https://github.com/ros2/demos demos && \
    cp -r demos/demo_nodes_cpp ${ROS_BUILD_ROOT}/src && \
    cp -r demos/demo_nodes_py ${ROS_BUILD_ROOT}/src && \
    rm -r -f demos"

# install dependencies using rosdep
sudo apt-get update
    cd ${ROS_BUILD_ROOT}
sudo rm -rf /etc/ros/rosdep/sources.list.d/20-default.list
sudo rosdep init
    rosdep update && \
    rosdep install --from-paths src --ignore-src --rosdistro ${ROS_DISTRO} -y --skip-keys "console_bridge fastcdr fastrtps rti-connext-dds-5.3.1 urdfdom_headers qt_gui" && \
    sudo rm -rf /var/lib/apt/lists/*

# build it!
sudo mkdir -p ${ROS_INSTALL_ROOT}
# sudo required to write build logs
sudo colcon build --merge-install --install-base ${ROS_INSTALL_ROOT} --packages-skip vision_msgs_rviz_plugins

cd $current_dir

mkdir -p misc_files/opt/ros/${ROS_DIST}/
cp -r /opt/ros/foxy/*  misc_files/opt/ros/${ROS_DIST}/
chmod -R 777 misc_files
