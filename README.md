# voxl-ros2-foxy

ROS2 Foxy build to install ros2 onto target voxl2

## Build and Install Instructions

1. Requires the qrb5165-emulator (found [here](https://gitlab.com/voxl-public/support/voxl-docker)) to run docker ARM image
    * (PC) ```cd [Path To]/voxl-mpa-to-ros2```
    * (PC) ```sudo voxl-docker -i voxl-emulator```
2. Build project binary:
    * (qrb5165-emulator) ```./install_build_deps.sh qrb5165 stable```
    * (qrb5165-emulator) ```./clean.sh```
    * (qrb5165-emulator) ```./build.sh```
    * (qrb5165-emulator) ```./make_package.sh```
    * (qrb5165-emulator) ```./deploy_to_voxl.sh ```

### Expose ros2 to bash

Run the following commands(on voxl2):

Source the ros2 foxy setup script:

```source /opt/ros/foxy/setup.bash```

You are now finished and can run ros2 nodes.
